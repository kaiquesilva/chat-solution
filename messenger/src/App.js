import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <section className="App-notifications">
            <article className="App-live-requests">
              <i className="App-notification-dialog-icon"></i>
              <p className="App-live-requests-label">
                24 Online requests
              </p>
            </article>
            <article className="App-alive-requests">
              <i className="App-notification-envelope-icon"></i>
              <p className="App-alive-requests-label">
                5 Offline requests
              </p>
            </article>
          </section>
        </header>
        <section className="App-messages">
          <article className="App-sender">
            <p className="App-message">
              Hello
            </p>
            <p className="App-message">
              I need some help to sort out the issue on my website
              Right now my website is down :(
            </p>
          </article>
          <article className="App-receiver">
            <p className="App-message">
              Hi Alice
            </p>
            <p className="App-message">
              Could you please tell me your customer ID?
            </p>
          </article>
          <article className="App-sender">
            <p className="App-message">
              Ok. thanks! Let me quickly look into this
            </p>
          </article>
        </section>
        <section className="App-message-controls">
          <form className="App-message-control">
            <textarea></textarea>
            <button>Send</button>
            <button>Send file</button>
          </form>
        </section>
      </div>
    );
  }
}

export default App;
